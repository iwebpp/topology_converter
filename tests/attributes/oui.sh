#!/usr/bin/env bash
set -e

cp ./examples/2switch_auto_mgmt.dot topology.dot
cat topology.dot
python3 ./topology_converter.py topology.dot -p libvirt --oui '123456'
leaf01Block=`sed -n '/DEFINE VM for leaf1/,/DEFINE VM for/p' < Vagrantfile`
echo $leaf01Block | grep '12:34:56'
echo $leaf01Block | grep -v '48:b0:2D'